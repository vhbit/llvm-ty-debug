extern crate rustc_llvm;

use rustc_llvm as llvm;

fn main() {
    println!("Hello, world!");

    unsafe {
        let data_layout = "e-p:32:32-i1:32:32-i8:32:32-i16:32:32-f64:32:32-v64:32:64-v128:32:128-a:0:32-n32-S32";
        let td = llvm::mk_target_data(data_layout);
        let ctx = llvm::LLVMContextCreate();
        let lmod = "test".with_c_str(|buf| {
            llvm::LLVMModuleCreateWithNameInContext(buf, ctx)
        });
        data_layout.with_c_str(|buf| {
            llvm::LLVMSetDataLayout(lmod, buf);
        });
        "thumbv7-apple-ios".with_c_str(|buf| {
            llvm::LLVMRustSetNormalizedTarget(lmod, buf);
        });
        let ty = llvm::LLVMInt8TypeInContext(ctx);
        let t = llvm::LLVMSizeOfTypeInBits(td.lltd, ty);
        let t2 = llvm::LLVMABISizeOfType(td.lltd, ty) * 8;
        println!("Real size {} abi size {}", t, t2);
    }
}
